#!/bin/bash

HOME_PATH="$(dirname "$(realpath "$0")")"

docker run -it -d --rm -h racket --name pp-racket \
	-e "DISPLAY=${DISPLAY}" -v "/tmp/.X11-unix:/tmp/.X11-unix:rw" \
	pp-racket