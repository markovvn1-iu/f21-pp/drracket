FROM ubuntu:20.04


ARG USER_UID=1000
ARG USER_GID=1000


ENV \
  TERM=xterm-256color \
  DEBIAN_FRONTEND=noninteractive \
  USER_NAME=app \
  USER_UID=$USER_UID \
  USER_GID=$USER_GID


RUN set -ex \
	&& echo "# deb-src http://ppa.launchpad.net/plt/racket/ubuntu focal main" > /etc/apt/sources.list.d/plt-ubuntu-racket-focal.list \
	&& apt update \
	&& apt-get -qq install sudo racket ca-certificates \
	&& apt-get -qq clean \
    && rm -rf /var/lib/apt/lists/* \
    \
    # Disable password for sudo
  	&& sed "s/^%sudo.*$/%sudo ALL=(ALL:ALL) NOPASSWD:ALL/g" -i /etc/sudoers


# Configure user and workspace
RUN set -ex \
  # Creating a user
  && groupadd -g ${USER_GID} ${USER_NAME} \
  && useradd -m -d /home/${USER_NAME} -u ${USER_UID} -s /bin/bash -g ${USER_NAME} -G sudo ${USER_NAME}


WORKDIR /src
USER ${USER_NAME}

ENTRYPOINT ["drracket"]